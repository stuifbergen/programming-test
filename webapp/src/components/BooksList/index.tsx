import React from "react";
import styles from "./styles.module.scss";
import { Book } from "../../types/Book";
import BookItem from "../BookItem";

type Props = {
  books: Book[];
};

const BooksList: React.FC<Props> = ({ books }) => {
  return (
    <div className={styles["items"]}>
      {books.map((book, key) => (
        <div key={key} className={styles["item"]}>
          <BookItem book={book} />
        </div>
      ))}
    </div>
  );
};

export default BooksList;
