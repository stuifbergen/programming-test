import React from "react";
import styles from "./styles.module.scss";
import { Book } from "../../types/Book";
import Image from "next/image";

type Props = {
  book: Book;
};

const BookItem: React.FC<Props> = ({ book }) => {
  return (
    <div className={styles["container"]}>
      <img className={styles["img"]} src={book.image} alt={book.name} />
      <p className={styles["name"]}>{book.name}</p>
    </div>
  );
};

export default BookItem;
