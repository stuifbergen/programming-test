export interface Book {
  id: string;
  name: string;
  image: string;
}
