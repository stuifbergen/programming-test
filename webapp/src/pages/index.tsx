import { useQuery, gql } from "@apollo/client";
import BooksList from "../components/BooksList";
import { Book } from "../types/Book";

export default () => {
  const { loading, error, data } = useQuery<{ books: Book[] }>(GET_BOOKS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error {error.message}</p>;

  const books = data ? data.books : [];

  return <BooksList books={books} />;
};

const GET_BOOKS = gql`
  query GetBooks {
    books {
      id
      name
      image
    }
  }
`;
