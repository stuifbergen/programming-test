# Programming Test

## How to run

### Initial setup

Execute `yarn` to install dependencies

### Start

`yarn start`: To start both the api and webapp  
`yarn api`: To start only the api  
`yarn webapp`: To start only the webapp

Go to http://localhost:4000 to see the api's graphql sandbox.  
Go to http://localhost:3000 to see the webapp.  

## Task

Create the following features:

- A new page where you can view the information of one book
  - You should be able to click a book in the books list, and get redirected to this page
  - The following information should be shown on this page: "book name", "book cover image" and "author name"
  - You will need to create a new Graphql query endpoint for this
- A form to create a new book
  - On the books list page you should add a button somewhere that redirects to this form to create a new book
  - You should be able to input the following information for the new book: "book name", "book cover image" and "author name"
  - You don't need to make an image uploader. Just inputting a string to an url of an image is enough.
  - For author you should be able to select an existing author or create a new author
  - You will need to create a new Graphql mutation endpoint for this
