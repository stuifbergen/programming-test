import getBooks from "./commands/get-books";

export default {
  Query: {
    books: getBooks,
  },
};
