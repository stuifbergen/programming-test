import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";
import schema from "./schema";
import resolvers from "./resolvers";
import { connectToDB } from "./db";

async function start() {
  await connectToDB();

  const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
  });
  const { url } = await startStandaloneServer(server, {
    listen: { port: 4000 },
  });
  console.log(`🚀 Server listening at: ${url}`);
}

start();
