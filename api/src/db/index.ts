import { DataSource } from "typeorm";

const AppDataSource = new DataSource({
  type: "sqlite",
  database: __dirname + "/data.db",
  entities: [__dirname + "/entities/*{.ts,.js}"],
});

export default AppDataSource;

export const connectToDB = async () => {
  await AppDataSource.initialize()
    .then(() => console.log("Connected to the DB"))
    .catch((err) => console.error("Could not connect to the DB", err));
};
