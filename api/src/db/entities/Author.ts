import {
  Entity,
  PrimaryGeneratedColumn,
  Column
} from "typeorm";

@Entity({ name: "authors" })
export class Author {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  name: string;
}
