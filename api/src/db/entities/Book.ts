import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity({ name: "books" })
export class Book {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  image: string;

  @Column({ name: "author_id" })
  authorId: string;
}
