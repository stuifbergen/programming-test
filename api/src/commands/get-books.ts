import db from "../db";
import { Book } from "../db/entities/Book";

export default async function getBooks() {
  return db.getRepository(Book).find();
}
