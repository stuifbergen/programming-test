import gql from "graphql-tag";

export default gql`
  type Book {
    id: Int
    name: String
    image: String
  }

  type Query {
    books: [Book]
  }
`;
